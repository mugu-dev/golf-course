# Code Golf Rules and Regulations

## Target System

**Container**: `registry.gitlab.com/mugu-dev/golf-course:latest-tee`

**Hardware**: Intel Haswell, 2 CPU cores, 2GB RAM

**Language**: ISO C17

**Compiler**: LLVM/Clang

**Locale**: en_US.UTF-8

## Compiling

Code will be built using the following commands unless otherwise specified in the instructions of a particular task:

For smallest binary footprint:
```sh
clang -std=c17 -Os -march=native -o footprint main.c -lm
```

For all other metrics:
```sh
clang -std=c17 -O3 -march=native -mtune=native main.c -lm
```

> NOTE: Source files should be UTF-8 encoded to ensure proper compilation and scoring.

## Timing
TBD

## Rounds

Each "round" of code golf will be comprised of either 9 or 18 tasks, called "holes".

While a given player may score higher or lower than others on an individual hole, the sum total of their scores for each hole determines the winner of the round.

## Scoring
All holes will be scored on three metrics:
1. [**Source Footprint**](#source-footprint)
1. [**Binary Footprint**](#binary-footprint)
1. [**Walltime**](#walltime)

Some holes will also include [bonus metrics](#bonus-scoring) selected from a pool of predetermined options.

### Source Footprint

The file size of the source code, _in bytes_. Smaller is better.

- Non-quoted whitespace will be subtracted from the total score.
- Multi-byte characters are allowed; ALL bytes used to represent the character will be counted.

### Binary Footprint

The file size of the compiled binary, _in bytes_. Smaller is better.

- The size will be determined using `du -b --apparent-size` on the resulting binary.
- The size will be normalized by subtracting the size of the smallest compilable program (i.e. `int main(){}`) from the total program size.

### Bonus scoring

TBD

Some holes will be scored using additional bonus metrics.

Bonus metrics are irregular metrics which may be added or subtracted from the player's score. Subtractive metrics **(-)** reduce the player's score, while additive metrics **(+)** penalize the player and increase their score.

Example bonus metrics:
- Highest level of `-Werror` without failure. ($`-5`$) for `-Werror`, ($`-10`$) for `-Wall -Werror`, ($`-15`$) for `-Weverything -Werror`
- Number of emojis in source code. ($`-5 \times N`$)
- Number of preprocessor statements. ($`+N`$)
- Total number of syscalls _as detected by `strace`_. ($`+N`$)
- Walltime. ($`+N`$) precision TBD
- Most "on" bits in SHA256 checksum of `main.c`. ($`-15`$) for top player
